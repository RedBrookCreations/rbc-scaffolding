alias ls='ls -A --color'
alias gl="git log --max-count=15 --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"


ta='~/rbc/projects/rbc-scaffolding/templates/angular'
tc='~/rbc/projects/rbc-scaffolding/templates/config'
