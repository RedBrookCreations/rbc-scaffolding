import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as ExampleReducer from '../reducers/example.reducers';
import * as Model from '../../models';

import { getModuleState } from './index';
import { getRouterParamId } from '@core/store/selectors';

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = ExampleReducer.exampleAdapter.getSelectors();

export const getExampleState = createSelector(
  getModuleState,
  (state) => state.example
);

export const getArrayOfExamples = createSelector(
  getExampleState,
  (exampleState): Model.ExampleData[] => {
    const entities = exampleState.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(example => example.data);
  }
);

export const getExampleEntities = createSelector(
  getExampleState,
  (state) =>  state.entities
);

export const getSelectedExample = createSelector(
    getExampleState,
    (state) => state // state.selected
);

export const getSelectedExampleFromRouteParams = createSelector(
  getRouterParamId,
  getExampleEntities,
  (id: number, entities): Model.ExampleData => entities[id] ? entities[id].data : null
);
