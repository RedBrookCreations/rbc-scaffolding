# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh
. ~/rbc-scaffolding/scripts/user-input.sh

# Set Variables
checkForCorrectDirectory "/src/app"
echoQuestionUser "What will be the name of this module?"
read module

# Method For Adding a Buffer File To a Directory
addBufferFileToDirectory() {
	directory=$1

	cd $directory
	touch index.ts
	echo "export const ${directory} = [

];" > index.ts

	echoCreateFile index.ts
	cd ..

}

# Create Directory
mkdir $module || echoError "failed to create directory"
echoCreateFolder $module
cd $module || echoFailedAction "Could not enter ${module} directory"
echoChangedDirectory

# Add Module Folders
mkdir forms components containers store models
echoCreateFolder "forms/" && addBufferFileToDirectory forms
echoCreateFolder "components/" && addBufferFileToDirectory components
echoCreateFolder "containers/" && addBufferFileToDirectory containers
echoCreateFolder "models/" && touch models/index.ts
echoCreateFile "models/index.ts"

# add state model to the models index file
echo "// prettier-ignore
export interface ${module^}State {
};" > models/index.ts

# Add Store Folders and Populate with Buffers
echoCreateFolder "store/"
cd store || eccoError "failed to change to store directory"
echoChangedDirectory


mkdir actions && echoCreateFolder "actions/"
mkdir reducers && echoCreateFolder "reducers/"
mkdir effects && echoCreateFolder "effects/"
mkdir selectors && echoCreateFolder "selectors/"

# Add buffer files to each store folder
touch actions/index.ts selectors/index.ts reducers/index.ts effects/index.ts
echoCreateFile "actions/index.ts"
echoCreateFile "reducers/index.ts"
echoCreateFile "effects/index.ts"
echoCreateFile "selectors/index.ts"

# Add comment so that sed works in actions file
echo "// delete this line" > actions/index.ts

# Add additinal information into effects index.ts
echo "// prettier-ignore
export const effects = [
];" > effects/index.ts || echoFailedAction "Could Not Add export array into effects/index.ts"

# Add additional information into reducers index.ts
echo "import { ActionReducerMap } from '@ngrx/store';
import { ${module^}State } from '../../models';

// prettier-ignore
export const reducers: ActionReducerMap<${module^}State> = {
};" > reducers/index.ts || echoFailedAction "Could reducer object into reducers/index.ts"

# Add additional information into the selectors buffer
echo "import { createFeatureSelector } from '@ngrx/store';
import { ${module^}State } from '../../models';
export const get${module^}State = createFeatureSelector<${module^}State>('${module}');" > selectors/index.ts || echoFailedAction "could not add state selector to selectors/index.ts"

# Navigate Back to Root of Module
cd ..
echoChangedDirectory

# Add Module and Route
touch "${module}.module.ts" routes.ts
echoCreateFile "${module}.module.ts"
echoCreateFile "routes.ts"

cat ~/rbc-scaffolding/templates/angular/modules/crud > "${module}.module.ts"
replaceCases example ${module} "${module}.module.ts"
cat ~/rbc-scaffolding/templates/angular/routes > "routes.ts"
sed -i "s/Example/${module^}/g" "${module}.module.ts"

# Add feature to main routes file in order to be Lazy Loaded
cd ..
echoChangedDirectory
sed -i "$ i { path: 'example', loadChildren: 'example/example.module#ExampleModule' }," routes.ts
sed -i "s/Example/${module^}/g" routes.ts
sed -i "s/example/${module}/g" routes.ts
echoUpdateFile "routes.ts"
