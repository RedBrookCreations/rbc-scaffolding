# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh

# ====================== SET VARIABLES ========================================
echoQuestionUser "What is the name of this Container?(use camelCase)"
read containerName
echoQuestionUser "What is the name of the module you are adding this container to?"
read moduleName
# containerName=$1
# moduleName=$2

containerFolder="${containerName}"
containerFile="${containerName}.container"

# Set VariableNames for Template Paths
containerHTMLTemplate=~/rbc-scaffolding/templates/angular/containers/list/html
containerTsTemplate=~/rbc-scaffolding/templates/angular/containers/list/ts
containerScssTemplate=~/rbc-scaffolding/templates/angular/containers/list/scss

# ====================== PERFORM OPERATIONS ========================================
cd containers || echoError "cannot navigate to containers directory"

# Create Directory and navigate to it
mkdir $containerFolder && echoCreateFolder $containerFolder
cd $containerFolder && echoChangedDirectory

# create files
touch "${containerFile}.scss" && echoCreateFile "${containerFile}.scss"
touch "${containerFile}.html" && echoCreateFile "${containerFile}.html"
touch "${containerFile}.ts" && echoCreateFile "${containerFile}.ts"

# Add Content From Template Into Each File
cat ${containerHTMLTemplate} > "${containerFile}.html"
cat ${containerScssTemplate} > "${containerFile}.scss"
cat ${containerTsTemplate} > "${containerFile}.ts"


# ====================== REPLACE ALL PLACEHOLDERS ========================================
## In TypeScript File
replaceCases placeholder ${containerName} "${containerFile}.ts"
# sed -i "s/PLACEHOLDER/${containerName^^}/g" "${containerFile}.ts"
# sed -i "s/Placeholder/${containerName^}/g"  "${containerFile}.ts"
# sed -i "s/placeholder/${containerName}/g" "${containerFile}.ts"

## In Html File
replaceCases placeholder ${containerName} "${containerFile}.html"
# sed -i "s/PLACEHOLDER/${containerName^^}/g" "${containerFile}.html"
# sed -i "s/Placeholder/${containerName^}/g" "${containerFile}.html"
# sed -i "s/placeholder/${containerName}/g" "${containerFile}.html"


# ====================== ADD IMPORT AND EXPORT TO INDEX.TS FILE ========================================
cd .. && echoChangedDirectory
prependString "import { ${containerName^}Component } from './${containerFolder}/${containerFile}';" index.ts
appendStringOneLineBeforeLast "$ i ${containerName^}Component," index.ts
echoUpdateFile index.ts
# sed -i "1 i import { ${containerName^}Component } from './${containerFolder}/${containerFile}';" index.ts
# sed -i "$ i ${containerName^}Component," index.ts



# ====================== ADD ROUTES ==============================================================
# Add route to index.ts file
cd .. && echoChangedDirectory
sed -i "1 i import { ${containerName^}Component } from './${containerFolder}/${containerFile}';" routes.ts
sed -i "$ i { path: '${containerName}', container: ${containerName^}Component }," routes.ts
echoUpdateFile routes.ts
