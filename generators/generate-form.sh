# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh
. ~/rbc-scaffolding/scripts/user-input.sh

# ======================================== Set Variables ========================================
checkForCorrectDirectory "root of the module you are adding the form to"
echoQuestionUser "What will be the name of this form?(use camelCase)"
read formName
chooseAFormField

folderName="${formName}Form"

formHtml="${formName}.component.html"
formScss="${formName}.component.scss"
formTs="${formName}.component.ts"

formTsTemplate=~/rbc-scaffolding/templates/angular/forms/ts
formHTMLTemplate=~/rbc-scaffolding/templates/angular/forms/html
formScssTemplate=~/rbc-scaffolding/templates/angular/forms/scss

startingDirectory=$(pwd)


# ======================================== Create Folder and Files ========================================
# Navigate to Forms directory
cd forms || echoError "Cannot navigate to forms directory"

# Create Folder
mkdir "${folderName}" && echoCreateFolder "${folderName}"

# cd into new folder
cd ${folderName} && echoChangedDirectory

# Create Files
touch "$formHtml" && echoCreateFile "${formHtml}"
touch "$formScss" && echoCreateFile "${formScss}"
touch "$formTs" && echoCreateFile "${formTs}"



# ======================================== Modify Form Files ========================================
# Add the templates to the files So they have Content
cat ${formTsTemplate} > "${formTs}"
cat ${formHTMLTemplate} > "${formHtml}"
cat ${formScssTemplate} > "${formScss}"

# Find and Replace 'example' with the ${formName}
/bin/find . -type f -exec sed -i "s/example/${formName}/g" {} +
/bin/find . -type f -exec sed -i "s/Example/${formName^}/g" {} +



# ======================================== Update index.ts with an Export ========================================
# Add the import and export to the index.ts buffer file
cd ..
sed -i "$ i ${formName^}FormComponent," index.ts
prependString "import { ${formName^}FormComponent } from './${folderName}/${formTs}'" index.ts
echoUpdateFile index.ts

cd ..
