# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh

# ====================== SET VARIABLES ========================================
echoQuestionUser "What is the name of this component?"
read componentName
# componentName=$1

componentFolder="${componentName}"
componentFile="${componentName}.component"

# Set VariableNames for Template Paths
componentHTMLTemplate=~/rbc-scaffolding/templates/angular/components/list/html
componentTsTemplate=~/rbc-scaffolding/templates/angular/components/list/ts
componentScssTemplate=~/rbc-scaffolding/templates/angular/components/list/scss



# ====================== PERFORM OPERATIONS ========================================
cd components || echoError "wrong directory"

# Create and navigate to directroy
mkdir $componentFolder && echoCreateFolder $componentFolder
cd $componentFolder && echoChangedDirectory

# Create Files
touch "${componentFile}.scss" && echoCreateFile "${componentFile}.scss"
touch "${componentFile}.html" && echoCreateFile "${componentFile}.html"
touch "${componentFile}.ts" && echoCreateFile "${componentFile}.ts"

# Add Content From Template Into Each File
cat ${componentHTMLTemplate} > "${componentFile}.html"
cat ${componentScssTemplate} > "${componentFile}.scss"
cat ${componentTsTemplate} > "${componentFile}.ts"



# ====================== REPLACE PLACEHOLDERS ========================================
## In TypeScript File
replaceCases data $componentName "${componentFile}.ts"

## In Html File
replaceCases data $componentName "${componentFile}.html"


# ====================== ADD IMPORT AND EXPORT TO INDEX.TS FILE ========================================
cd .. && echoChangedDirectory
prependString "import { ${componentName^}Component } from './${componentFolder}/${componentFile}';" index.ts
appendStringOneLineBeforeLast "${componentName^}Component," index.ts
echoUpdateFile index.ts

cd ..
