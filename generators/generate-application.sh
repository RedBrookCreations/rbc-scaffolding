. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh
. ~/rbc-scaffolding/scripts/user-input.sh

# Initialize Variables For Readability
echoQuestionUser "What is the title of this application?"
read title


# Grab the Current RBC Seed Application
echoStartingAction "Clone RBC Seed from Git and Change it to ${title}"
git clone https://gitlab.com/redbrookcreations/seed.git
echoFinishedAction 'Cloned RBC Seed to current Directory'

# Change the directory name
mv seed "$title" || echoError "Failed To Change Title"
echoCreateFolder "${title}"

# Cd into the directory
cd "$title" || echoError "Failed to enter directory"
echoChangedDirectory

# Remove the .git folder
rm -rf .git
echoSuccess "removed old .git directory"

# Remove the old README
rm README.md || echoError "Failed to remove old readme.md"
echoSuccess "removed old README"

# Remove package-lock.json
rm package-lock.json || echoError "Failed to remove old package-lock.json"
echoSuccess "removed old package-lock.json"

# Get Primary Brand Color From user
echoQuestionUser "What would your like your primary brand color(enter the number by the color)?"
chooseAColor
replace "mat-indigo, 500" "mat-${newColor}" ./src/scss/_themes.scss
echoSuccess "Primary Color is ${newColor}"

# Get Secondary Brand Color From user
echoQuestionUser "What would you like your secondary color to be?(enter the number by the color)"
chooseAColor
replace "mat-indigo, 300" "mat-${newColor}" ./src/scss/_themes.scss
echoSuccess "Secondary Color is ${newColor}"

# Recursively find and replace all instances of 'seed'
# find . -type f -exec sed -i "s/seed/${title}/g" {} + || echoError "could not find adn replace"
replaceRecursively "seed" ${title} || echoError "Could not find and replace"
echoSuccess "Replaced all instances of 'seed' with ${title}"

# Initialize New Git Repo, Create a Project on Gitlab, and Set the new project as the remote
# git init
# git add .
# git commit -m "Initializing New Project From RBC Seed"
# git push --set-upstream https://gitlab.com/redbrookcreations/rbc-${title}.git master
# git remote add origin https://gitlab.com/redbrookcreations/rbc-${title}.git
echoSuccess "Created a new git repo for ${title} with prefix 'rbc'"

# npm instalL
echoStartingAction "Begin NPM Install"
# npm install --silent
echoSuccess "completed npm install"

# Tell User Next Steps
touch src/environments/credentials.ts && echoCreateFile "src/environments/credentials.ts"
cat ~/rbc-generators/templates/angular/config/credentials > src/environments/credentials.ts && echoUpdateFile "src/environments/credentials.ts"

echoWarning "IMPORTANT: Make sure to enable email login, google oAuth, and facebook oAuth in firebase auth"
echoWarning "IMPORTANT: Make sure to add a server location to this project(located in firebase console settings)"
echoWarning "IMPORTANT: Make sure to add a logo to the application in assets/images/"

# Open up the application in VSCODE
code .
code src/environments/credentials.ts

# serve the application
ng serve
