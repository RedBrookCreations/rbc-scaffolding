# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh

# Create Variables
apiName=$1
moduleName=$2

# Generate Store && Models
source ~/rbc-scaffolding/generators/generate-store.sh ${apiName} ${moduleName}
echoFinishedAction "DB Model, API, and State Management System created"

# Generate Container
source ~/rbc-scaffolding/generators/generate-container.sh ${apiName} ${moduleName}
echoFinishedAction "Container Created, Route Created"

# Generate Component
source ~/rbc-scaffolding/generators/generate-component.sh "${apiName}List" ${moduleName}
echoFinishedAction "Component List Has Been Created"

# Generate Forms
source ~/rbc-scaffolding/generators/generate-form.sh "${apiName}Filter"
echoFinishedAction "Filting Form has been Created"
source ~/rbc-scaffolding/generators/generate-form.sh "${apiName}Update"
echoFinishedAction "Update Form has been created"
source ~/rbc-scaffolding/generators/generate-form.sh "${apiName}Create"
echoFinishedAction "create from has been created"
