# Source Helper Scripts
. ~/rbc-scaffolding/scripts/pretty-echo.sh
. ~/rbc-scaffolding/scripts/sed-helper.sh
. ~/rbc-scaffolding/scripts/user-input.sh

# ======================================== Set Variables ========================================
echoQuestionUser "What is the name of the module you are adding this store to?"
read module
checkForCorrectDirectory "root ${module}"
echoQuestionUser "What will be the name of your API?"
read api
# api=$1
# module=$2

apiModelFile="${api}.models.ts"
apiModelTemplate=~/rbc-scaffolding/templates/angular/models/crud



# ======================================== Add Files To Store Folders ========================================
# navigate to Store directory
cd store || echoError "can't navigate to store directory"

# Add Files To Store and Add imports to Buffer
addFileToStoreFolder() {
	folder=$1
	storeFile="${api}.${folder}.ts"
	storeFileTemplate=~/rbc-scaffolding/templates/angular/store/${folder}

	#cd into folder
	cd $folder || eccoError "unable to navigate to ${folder} directory"
	echoChangedDirectory

	# create file
	touch $storeFile && echoCreateFile $storeFile

	# add contents of template into file
	cat $storeFileTemplate > $storeFile || eccoError "cannot add data from template to ${storeFile}"

	# Find and replace placeholder with new name
	replaceCases "example" $api $storeFile

	# add export to buffer file
	prependString "export * from './${api}.${folder}'" index.ts
	echoUpdateFile "index.ts"

	cd ..
}

# Add Main Files that Create a REDUX Store
addFileToStoreFolder actions
addFileToStoreFolder effects
addFileToStoreFolder reducers
addFileToStoreFolder selectors

# ======================================== Update index.ts Files ========================================
# Add Additional Import/Export to reducer
prependString "import { ${api}Reducer } from './${api}.reducers'" reducers/index.ts
appendStringOneLineBeforeLast "${api}: ${api}Reducer," reducers/index.ts
echoUpdateFile "reducers/index.ts"

# Add Additional Import/Export to effects
prependString "import { ${api^}Effects } from './${api}.effects'" effects/index.ts
appendStringOneLineBeforeLast "${api^}Effects," effects/index.ts
echoUpdateFile "effects.index.ts"

# Replace Modules name in selectors file
replaceCases module ${module} "selectors/${api}.selectors.ts"



# ======================================== Create New Model ========================================
# navigate to directory
cd ../models || echoError "could not enter models directory"
echoChangedDirectory

# Create Model File
touch $apiModelFile && echoCreateFile $apiModelFile

# Add Template Model TO File
cat $apiModelTemplate > $apiModelFile

# Replace Placeholders
replaceCases example $api $apiModelFile

# Add export for model
prependString "export * from './${api}.models'" index.ts

# Add export for apiState
prependString "import { ${api^}State } from '../store/reducers/${api}.reducers'" index.ts

# add store to apiState
appendStringOneLineBeforeLast "${api}: ${api^}State," index.ts
echoUpdateFile index.ts

cd ..
