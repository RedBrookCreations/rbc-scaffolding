# This is a Scaffolding System Meant To Speed Up Development

## Capabilities

-   Generate an Angular application seed based on the [RBCSeed](https://gitlab.com/redbrookcreations/seed)
-   Generate a Lazy Loaded Module within an RBCseed based application
-   Generate a Page with a route within a module
-   Generate a Form within a module
-   Generate a Component within a module

## How To Use

1. Place contents of this folder into the system root
2. Place aliases for each generator into your .bashrc file. There will be a generator for this soon. Make sure you are using git bash if you are on windows.
3. Don't fuck up when typing the commands.
4. pray

## How It Works

1. The template folder holds files with boilerplate code
2. The generater folder holds scripts that create new files, insert code from the templates, and replace variables within that code.

## File Structure

-   **generators:** bash scripts that create folders and files, add text to them, and then run scripts on them to modify
-   **templates:** text files with whatever boilerplate code you are wanting to past and modify
    <!-- -   **scripts:** bash scripts that do something useful -->

### Why not use Yeoman or Angular Schematics?

Creating a yeoman generator or a massive schematic is overkill when you are basically just copying, pasting, and replacing a few variables. You don't need a bunch of prompts and javascript, you just need a faster way to add some boilerplate to your application.

### Why did you write this in bash and not python / ruby / etc?

Bash looks like gibberish but it's the best at what it does: light automation. The goal is not to have an application built for us, but to remove the parts of front end development that are the same across every large application.(auth system, state management system, etc)

## How To Extend

1. Create a file in /templates with a generic name that describes what will be scaffolded.
2. Place the code you want to scaffold into this file. If there is a variable that needs to be replaced, replace it with 'example'
3. Create a bash script in /generators that creates the files/folders required.
4. Use the `cat {template} > {file2.txt}` operator to place the code from template into file2.txt
5. Use `sed -i "s/example/${userInput}/g" {file.txt}` in order to find and replace 'example' with {userInput} inside of {file.txt}
6. Use `sed -i "$ a {code to append}" {file.txt}` in order to append or insert code onto the last line of a file. replacing `a` with `i` will append it to the second to last line of the file.
7. Use `sed -i "1 i {code to add}" {file.txt}` to append text to the first line of the file

### BE CAREFUL

-   `sed` will not work unless there is something inside of the file
-   `>` will replace anything that is inside of the file with the other files contents
-   if you add an extension to the template files, the `>` operator no longer works
